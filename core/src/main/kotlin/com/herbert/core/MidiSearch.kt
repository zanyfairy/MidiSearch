package com.herbert.core

import kotlinx.coroutines.*
import java.io.File
import java.util.*
import java.util.concurrent.ConcurrentLinkedQueue
import java.util.concurrent.Executors
import java.util.concurrent.atomic.AtomicInteger
import java.util.logging.Logger

class MidiSearch(private val sourcePath: String) {
    private val logger = Logger.getLogger("MidiSearch")

    private val dataSource = DataReader(File(sourcePath))
    private val coroutineDispatcher = Executors.newFixedThreadPool(16).asCoroutineDispatcher()
    private var buildingState = AtomicInteger(0)    // 0: 正常； 1: 增量添加； 2: 全量重构

    private var job: Job? = null

    data class Result(
        val id: Int,
        val rate: Double,
    )

    fun isEmpty(): Boolean {
        return dataSource.getSources().isEmpty()
    }

    fun isReady(): Boolean {
        return !isEmpty() && buildingState.get() != 2
    }

    fun isBuilding(): Boolean {
        return buildingState.get() != 0
    }

    fun reload() {
        dataSource.reload()
    }

    /**
     * 通过csv文件增量生成数据仓库
     * source: csv文件，第一列为曲谱ID，第二列为ccmz或midi路径（本地文件或网址）
     * rebuild: 是否重新生成数据仓库
     */
    fun buildDataWarehouse(source: String, rebuild: Boolean = false) {
        logger.info("buildDataWarehouse start")
        buildingState.set(if (isEmpty() || rebuild) 2 else 1)

        job = GlobalScope.launch {
            val dataWriter = DataWriter(File(sourcePath), buildingState)
            dataWriter.buildDataWarehouse(source)
        }
        runBlocking {
            job?.join()
        }

        dataSource.reload()
        buildingState.set(0)
        logger.info("buildDataWarehouse end")
    }

    fun stopBuild() {
        logger.info("stopBuild start")
        buildingState.set(0)
        runBlocking {
            job?.join()
            job = null
            logger.info("stopBuild end")
        }
    }

    /**
     * 更新数据仓库
     */
    fun updateDataWarehouse(id: Int, path: String): Boolean {
        logger.info("updateDataWarehouse start")
        if (buildingState.get() != 0) {
            logger.warning("updateDataWarehouse cancel")
            return false
        }
        buildingState.set(1)
        val dataWriter = DataWriter(File(sourcePath), buildingState)
        val ret = dataWriter.addSource(id, path)
        dataWriter.done()
        logger.info("updateDataWarehouse end")
        return ret
    }

    fun searchText(text: String): List<Result> {
        return runBlocking {
            searchTextNG(text)
        }
    }

    fun searchMidiFile(midiFile: File): List<Result> {
        val text = MelodyParser.parseMidi(midiFile)
        return runBlocking {
            searchTextNG(text)
        }
    }

    private suspend fun searchTextNG(text: String): List<Result> {
        val startTime = Date().time
        val searchString = text.substring(0, Math.min(text.length, Constants.LshLen / 2))

        val miniHash = LSH.getInstance().calculate(searchString)
        val sources = dataSource.getSources()
        val candidates = ConcurrentLinkedQueue<Result>()

        GlobalScope.launch {
            //在父协程中创建子协程
            repeat(sources.size) { index ->
                val item = sources[index]
                //使用自定义线程池执行协程
                async(coroutineDispatcher) {
                    if (item.second.isEmpty()) {
                        logger.warning("empty source id:${item.first}")
                        return@async
                    }
                    val rate = item.second.maxOf { v ->
                        LSH.getInstance().compare(miniHash, v).toDouble() // / Math.sqrt(rate)
                    }
                    if (rate > 0.5) candidates.add(Result(item.first, rate))
                }
            }
        }.join()

        val results = candidates.sortedByDescending { it.rate }.take(5)
        results.forEach {
            logger.info("search id:${it.id},rate:${it.rate}")
        }
        logger.info("search cost:${Date().time - startTime} ms")
        return results
    }
}
