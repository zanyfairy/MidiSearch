package com.herbert.core

import org.apache.lucene.analysis.Analyzer
import org.apache.lucene.analysis.TokenStream
import org.apache.lucene.analysis.Tokenizer
import org.apache.lucene.analysis.ngram.NGramTokenizer
import org.codelibs.minhash.MinHash
import org.codelibs.minhash.analysis.MinHashTokenFilter
import java.util.concurrent.ConcurrentHashMap

class LSH(type: Int) {
    private val analyzer = ThreadLocal<Analyzer>()

    // The number of bits for each hash value.
    private val hashBit = 1

    // A base seed for hash functions.
    private val seed = 0

    // The number of hash functions.
    private val num = 1024

    // Analyzer for 1-bit 128 hash with custom Tokenizer.
    private val hashFunctions = MinHash.createHashFunctions(seed, num)

    companion object {
        private val instances = ConcurrentHashMap<Int, LSH>()
        fun getInstance(type: Int = 0): LSH {
            if (instances[type] == null) {
                instances[type] = LSH(type)
            }
            return instances[type]!!
        }
    }

    private fun getAnalyzer(): Analyzer {
        return this.analyzer.get() ?: let {
            val tokenizer: Tokenizer = NGramTokenizer(4, 7)
            val analyzer = object : Analyzer() {
                override fun createComponents(fieldName: String): TokenStreamComponents {
                    val stream: TokenStream = MinHashTokenFilter(tokenizer, hashFunctions, hashBit)
                    return TokenStreamComponents(tokenizer, stream)
                }
            }
            this.analyzer.set(analyzer)
            analyzer
        }
    }

    fun calculate(text: String): ByteArray {
        return MinHash.calculate(getAnalyzer(), text)
    }

    fun compare(source: ByteArray, target: ByteArray): Float {
        return MinHash.compare(source, target)
    }
}
