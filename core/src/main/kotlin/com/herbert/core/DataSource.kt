package com.herbert.core

import kotlinx.coroutines.*
import okhttp3.OkHttpClient
import okhttp3.Request
import org.apache.commons.csv.CSVFormat
import org.apache.commons.csv.CSVParser
import org.apache.commons.csv.CSVPrinter
import org.json.JSONObject
import java.io.*
import java.net.URL
import java.util.*
import java.util.concurrent.ConcurrentHashMap
import java.util.concurrent.Executors
import java.util.concurrent.atomic.AtomicInteger
import java.util.logging.Logger
import java.util.zip.ZipEntry
import java.util.zip.ZipInputStream

class DataReader(private val csvFile: File) {
    private val logger = Logger.getLogger("DataReader")
    private var sources = mutableListOf<Pair<Int, Array<ByteArray>>>()

    init {
        reload()
    }

    fun reload() {
        val data = mutableListOf<Pair<Int, Array<ByteArray>>>()
        logger.info("start load")
        val startTime = Date().time
        if (csvFile.exists()) {
            val fileReader = FileReader(csvFile)
            val csvParser = CSVParser(fileReader, CSVFormat.DEFAULT)
            for (record in csvParser) {
                val id = record.get(0).toInt()
                val vectors = Array<ByteArray>(record.size() - 1) {
                    Base64.getDecoder().decode(record.get(it + 1).toString())
                }

                data.add(Pair(id, vectors))
            }

            csvParser.close()
            fileReader.close()
        } else {
            logger.warning("data warehouse not found")
        }
        sources = data
        val cost = Date().time - startTime
        logger.info("end load cost $cost ms")
    }

    fun getSources(): List<Pair<Int, Array<ByteArray>>> {
        return sources
    }
}

class DataWriter(private val dataWarehouse: File, private val buildingState: AtomicInteger) {
    private val logger = Logger.getLogger("DataWriter")

    private val rebuild = buildingState.get() == 2
    private val sources = ConcurrentHashMap<Int, Array<ByteArray>>()
    private val okhttp = OkHttpClient()
    private val startTime = Date().time
    private val errors = ConcurrentHashMap<Int, String>()
    private val successes = ConcurrentHashMap<Int, String>()
    private var building = false

    init {
        logger.info("start")
    }

    private fun addMidi(id: Int, midiFile: File): Boolean {
        return FileInputStream(midiFile).use {
            addMidi(id, it)
        }
    }

    private fun addMidi(id: Int, inputStream: InputStream): Boolean {
        val melody = MelodyParser.parseMidi(inputStream)
        addTextNG(id, melody)
        return true
    }

    private fun addCCMZ(id: Int, inputStream: InputStream, score: Boolean = false): Boolean {
        return Reader.readCCMZ(inputStream, score)?.let { json ->
            addJson(id, json, score)
        } ?: false
    }

    private fun addJson(id: Int, json: String, score: Boolean = false): Boolean {
        if (score) {
            val lines = Reader.json2Lines(json)
            val melody = MelodyParser.extractLines(lines)
            addTextNG(id, melody)
        } else {
            val notes = Reader.json2Notes(json)
            val melody = MelodyParser.parseNotes(notes)
            addTextNG(id, melody)
        }
        return true
    }

    private fun addCCMZ(id: Int, ccmz: File, score: Boolean = false): Boolean {
        return FileInputStream(ccmz).use {
            addCCMZ(id, it, score)
        }
    }

    fun addSource(id: Int, path: String): Boolean {
        if (path.startsWith("http")) {
            val url = URL(path)
            val isCCMZ = if (url.path.endsWith(".ccmz")) {
                true
            } else if (url.path.endsWith(".mid")) {
                false
            } else {
                logger.warning("unsupported path:$path")
                return false
            }
            val request: Request = Request.Builder()
                .url(url)
                .build()
            try {
                val response = okhttp.newCall(request).execute()
                response.takeIf {
                    it.isSuccessful
                }?.body?.byteStream()?.let {
                    return if (isCCMZ) {
                        addCCMZ(id, it, true)
                    } else {
                        addMidi(id, it)
                    }
                } ?: run {
                    logger.warning("download failed path:$path")
                }
            } catch (e: Exception) {
                logger.warning("download failed path:$path")
                return false
            }
        } else if (path.startsWith("/")) {
            val file = File(path)
            if (file.exists()) {
                return if (path.endsWith(".ccmz")) {
                    addCCMZ(id, file)
                } else if (path.endsWith(".mid")) {
                    addMidi(id, file)
                } else {
                    logger.warning("unsupported path:$path")
                    false
                }
            } else {
                logger.warning("file not found:$path")
            }
        } else {
            logger.warning("unsupported path:$path")
        }
        return false
    }

    private fun addTextNG(id: Int, text: String) {
        logger.info("processing id:$id")
        val vectors = mutableListOf<ByteArray>()
        var pos = 0
        while (pos < text.length) {
            val str = if (pos + Constants.LshLen < text.length) {
                text.substring(pos, pos + Constants.LshLen)
            } else {
                text.substring(Math.max(0, text.length - Constants.LshLen))
            }
            logger.info("id:$id,str:$str")
            val minHash = LSH.getInstance().calculate(str)
            vectors.add(minHash)

            pos += Constants.LshLen / 2
        }
        vectors.toTypedArray().apply {
            sources[id] = this
        }
    }

    fun done() {
        if (building) {
            saveDataWarehouse()
            saveSuccessesAndErrors()

            val cost = Date().time - startTime
            logger.info("end cost $cost ms")
            building = false
        }
    }

    private fun saveDataWarehouse() {
        logger.info("saveDataWarehouse")
        val fileWriter = FileWriter(dataWarehouse, !rebuild)
        val csvPrinter = CSVPrinter(fileWriter, CSVFormat.DEFAULT)
        sources.forEach { item ->
            item.value.map { vectors ->
                Base64.getEncoder().encodeToString(vectors)
            }.toTypedArray().apply {
                csvPrinter.printRecord(item.key.toString(), *this)
            }
        }
        // 关闭CSVPrinter和FileWriter以保存文件
        csvPrinter.close(true)
        fileWriter.close()
    }

    private fun readDataSource(dataSource: String): List<Pair<Int, String>> {
        val fileReader = FileReader(dataSource)
        val csvParser = CSVParser(fileReader, CSVFormat.DEFAULT)
        val records = mutableListOf<Pair<Int, String>>()
        csvParser.records.forEach { record ->
            val id = record.get(0).toIntOrNull() ?: return@forEach
            val path = record.get(1)
            records.add(Pair(id, path))
        }
        return records
    }

    private fun readIds(file: File): Set<Int> {
        if (!file.exists()) {
            return setOf()
        }
        val fileReader = FileReader(file)
        val csvParser = CSVParser(fileReader, CSVFormat.DEFAULT)
        val records = mutableSetOf<Int>()
        csvParser.records.forEach { record ->
            val id = record.get(0).toIntOrNull() ?: return@forEach
            records.add(id)
        }
        return records
    }

    private fun readSuccesses(): Set<Int> {
        val file = File(dataWarehouse.parent, dataWarehouse.nameWithoutExtension + "_success.csv")
        return readIds(file)
    }

    private fun readErrors(): Set<Int> {
        val file = File(dataWarehouse.parent, dataWarehouse.nameWithoutExtension + "_error.csv")
        return readIds(file)
    }

    suspend fun buildDataWarehouse(dataSource: String) {
        if (building) {
            logger.warning("error: already building")
            return
        }
        building = true

        var records = readDataSource(dataSource)
        if (!rebuild) {
            val success = readSuccesses()
            records = records.filter { !success.contains(it.first) }
        }
        val coroutineDispatcher = Executors.newFixedThreadPool(16).asCoroutineDispatcher()
        val job = GlobalScope.launch {
            //在父协程中创建子协程
            repeat(records.size) { index ->
                val item = records[index]
                //使用自定义线程池执行协程
                async(coroutineDispatcher) {
                    if (buildingState.get() == 0) return@async

                    val ret = addSource(item.first, item.second)
                    if (ret) {
                        successes[item.first] = item.second
                    } else {
                        errors[item.first] = item.second
                    }
                }
            }
        }
        job.join()//等待子协程执行完毕
        coroutineDispatcher.close()

        done()
    }

    private fun writeRecords(file: File, records: Map<Int, String>) {
        if (records.isNotEmpty()) {
            val fileWriter = FileWriter(file, true)
            val csvPrinter = CSVPrinter(fileWriter, CSVFormat.DEFAULT)
            records.forEach { (id, path) ->
                csvPrinter.printRecord(id, path)
            }
            csvPrinter.close(true)
            fileWriter.close()
        }
    }

    private fun saveSuccessesAndErrors() {
        logger.info("saveSuccessesAndErrors, successes:${successes.size}, errors:${errors.size}")
        if (errors.isNotEmpty()) {
            val file = File(dataWarehouse.parent, dataWarehouse.nameWithoutExtension + "_error.csv")
            writeRecords(file, errors)
        }

        if (successes.isNotEmpty()) {
            val file = File(dataWarehouse.parent, dataWarehouse.nameWithoutExtension + "_success.csv")
            writeRecords(file, successes)
        }
    }
}
