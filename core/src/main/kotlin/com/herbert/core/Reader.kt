package com.herbert.core

import org.json.JSONObject
import java.io.ByteArrayInputStream
import java.io.File
import java.io.FileInputStream
import java.io.InputStream
import java.util.zip.ZipEntry
import java.util.zip.ZipInputStream

object Reader {
    fun json2Notes(json: String): List<MelodyParser.NoteEvent> {
        val root = JSONObject(json)
        val events = root.getJSONArray("events")
        val notes = mutableListOf<MelodyParser.NoteEvent>()
        for (i in 0 until events.length()) {
            val obj = events.getJSONObject(i)
            val tick = obj.getLong("tick")
            val arr = obj.getJSONArray("event")
            val eventTypeByte = arr.optInt(0)
            val nn = arr.optInt(1)
            val vv = arr.optInt(2)
            val eventType = eventTypeByte shr 4
            when (eventType) {
                0x08 -> {
                    notes.add(MelodyParser.NoteEvent(tick, MelodyParser.NoteEventType.Off, nn.toByte()))
                }

                0x09 -> {
                    if (vv == 0) {
                        notes.add(MelodyParser.NoteEvent(tick, MelodyParser.NoteEventType.Off, nn.toByte()))
                    } else {
                        notes.add(MelodyParser.NoteEvent(tick, MelodyParser.NoteEventType.On, nn.toByte()))
                    }
                }

            }
        }
        return notes
    }

    fun json2Lines(json: String): List<Pair<Int, Int>> {
        val lines = mutableListOf<Pair<Int, Int>>()

        val root = JSONObject(json)
        val parts = root.getJSONArray("parts")
        for (p in 0 until parts.length()) {
            val part = parts.getJSONObject(p)
            val measures = part.getJSONArray("measures")
            for (i in 0 until measures.length()) {
                val measure = measures.getJSONObject(i)
                val notes = measure.getJSONArray("notes")
                val cols = mutableMapOf<Int, Pair<Int, Int>>()
                for (j in 0 until notes.length()) {
                    val note = notes.getJSONObject(j)
                    val elems = note.optJSONArray("elems") ?: continue
                    val col = note.getInt("col")
                    val staff = note.getInt("staff") - 1
                    for (k in 0 until elems.length()) {
                        val elem = elems.getJSONObject(k)
                        val line = elem.getInt("line")
                        val last = cols[col]
                        cols[col] = last?.let {
                            maxOf(
                                it, Pair(staff, line)
                            ) { o1, o2 ->
                                if (o1.first != o2.first) o2.first - o1.first
                                else o1.second - o2.second
                            }
                        } ?: Pair(staff, line)
                    }
                }
                lines.addAll(cols.toSortedMap().values)
            }
            if (lines.isNotEmpty()) return lines
        }
        return lines
    }

    fun readCCMZ(file: File, score: Boolean = false): String? {
        FileInputStream(file).use { inputStream ->
            return readCCMZ(inputStream, score)
        }
    }

    fun readCCMZ(inputStream: InputStream, score: Boolean = false): String? {
        val bytes = inputStream.readBytes()
        val version = bytes[0].toInt()
        var data = bytes.copyOfRange(1, bytes.size)
        if (version == 2) {
            data = data.map {
                if (it % 2 == 0) it.inc() else it.dec()
            }.toByteArray()
        }
        val bis = ByteArrayInputStream(data)
        val zipInputStream = ZipInputStream(bis)
        var entry: ZipEntry?
        do {
            entry = zipInputStream.nextEntry
            if (entry != null) {
                if (!score && (entry.name == "midi.json" || entry.name.endsWith(".ccmid"))) {
                    return zipInputStream.readBytes().decodeToString()
                } else if (score && (entry.name == "score.json" || entry.name.endsWith(".ccxml"))) {
                    return zipInputStream.readBytes().decodeToString()
                }
            } else {
                break
            }
        } while (true)
        return null
    }
}
