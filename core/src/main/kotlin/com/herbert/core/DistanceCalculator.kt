package com.herbert.core

import kotlin.math.sqrt

class DistanceCalculator {

    private fun calcEuclideanVector(s: String): List<Float> {
        val octave = 1
        val size = octave * 12
        val bagOfWords = Array(size) { 0 }
        s.forEach { bagOfWords[it.code % (size)]++ }
        return bagOfWords.map { it.toFloat() / size }
    }

    private fun calcEuclideanDistance(s: String, t: String): Float {
        val v1 = calcEuclideanVector(s)
        val v2 = calcEuclideanVector(t)
        var acc = 0F
        for (i in v1.indices) {
            val diff = v1[i] - v2[i]
            acc += diff * diff
        }
        return sqrt(acc)
    }

    private fun calcLevenshteinDistance(s: String, t: String, bandWidth: Int = 0): Int {
        val len1 = s.length
        val len2 = t.length

        // 创建一个二维数组来存储Levenshtein距离
        val dp = Array(len1 + 1) { IntArray(len2 + 1) }

        // 初始化第一行和第一列
        for (i in 0..len1) {
            dp[i][0] = i
        }
        for (j in 0..len2) {
            dp[0][j] = j
        }

        // 填充矩阵
        for (i in 1..len1) {
            for (j in 1..len2) {
                val cost = if (s[i - 1] == t[j - 1]) 0 else 1
                val minDistance = minOf(
                    dp[i - 1][j] + 1,      // 插入操作
                    dp[i][j - 1] + 1,      // 删除操作
                    dp[i - 1][j - 1] + cost  // 替换操作
                )

                if (bandWidth > 0) {
                    // 考虑Sakoe-Chiba带的限制
                    val lowerBound = maxOf(1, j - bandWidth)
                    val upperBound = minOf(len2, j + bandWidth)
                    if (j in lowerBound..upperBound) {
                        dp[i][j] = minDistance
                    } else {
                        dp[i][j] = minDistance + bandWidth
                    }
                } else {
                    dp[i][j] = minDistance
                }
            }
        }

        // 最后一个矩阵元素包含Levenshtein距离
        return dp[len1][len2]
    }

    private fun MFSC(s: String, t: String): Int {
        val bandWidth = maxOf(20, minOf(s.length, t.length, 400) / 10)
        return calcLevenshteinDistance(s, t, bandWidth)
    }

    private fun transform400(s: String): String {
        return if (s.length <= 400) s
        else s.substring(0, 200).plus(s.substring(s.length - 200, s.length))
    }

    private fun MFSCFinal(s: String, t: String): Int {
        return MFSC(transform400(s), transform400(t))
    }

    private fun findClosestMatch(text: String, pattern: String): Int {
        val patternLength = pattern.length
        val textLength = text.length
        val maxDistance = patternLength // 假设最大距离为模式长度

        var minDistance = maxDistance
        var closestMatchIndex = -1

        for (i in 0..textLength - patternLength) {
            val substring = text.substring(i, i + patternLength)
            val distance = MFSC(substring, pattern)

            if (distance < minDistance) {
                minDistance = distance
                closestMatchIndex = i
            }

            // 早期终止：如果已经找到距离为0的匹配，可以立即返回
            if (minDistance == 0) {
                break
            }
        }

        return minDistance
    }

}
