package com.herbert.core

import kotlinx.coroutines.runBlocking
import java.io.File

fun createMidiSearch(): MidiSearch {
    val dataWarehouse = "/Users/Herbert/Downloads/dataWarehouse.csv"
    val dataSource = "/Users/Herbert/Downloads/dataSource.csv"

    val midiSearch = MidiSearch(dataWarehouse)
    if (midiSearch.isEmpty()) {
        midiSearch.buildDataWarehouse(dataSource)
    }
    return midiSearch
}

fun main(args: Array<String>) {

    val midiSearch = runBlocking {
        createMidiSearch()
    }
    midiSearch.updateDataWarehouse(100001, "/Users/Herbert/Downloads/01.mid")
    val files = arrayOf("01.mid", "02.mid", "03.mid", "04.mid", "05.mid")
//    val files = arrayOf("11.mid", "22.mid", "33.mid", "44.mid", "55.mid")
//    val files = arrayOf("test.mid", "test2.mid", "test3.mid", "123.mid")
    files.forEach {
        val path = "/Users/Herbert/Downloads/$it"
        println(path)
        val results = midiSearch.searchMidiFile(File(path))
    }

}
