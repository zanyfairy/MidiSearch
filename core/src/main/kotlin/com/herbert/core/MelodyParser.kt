package com.herbert.core

import java.io.File
import java.io.FileInputStream
import java.io.InputStream
import java.util.logging.Logger
import javax.sound.midi.MidiSystem
import kotlin.math.max
import kotlin.math.min

object MelodyParser {
    private val logger = Logger.getLogger("MelodyParser")

    enum class NoteEventType {
        Off, On
    }

    data class NoteEvent(
        val tick: Long,
        val type: NoteEventType,
        var note: Byte,
        var name: String = ""
    )

    private fun readNotesFromMidi(inputStream: InputStream): List<NoteEvent> {
        val sequence = MidiSystem.getSequence(inputStream)
        val tracks = sequence.tracks
        val notes = mutableListOf<NoteEvent>()
        for (track in tracks) {
            for (index in 0 until track.size()) {
                val event = track.get(index)
                val bytes = event.message.message
                if (bytes.isNotEmpty()) {
                    if ((bytes[0].toUInt() and 0xF0U) == 0x80U) {
                        notes.add(NoteEvent(event.tick, NoteEventType.Off, bytes[1]))
                    } else if ((bytes[0].toUInt() and 0xF0U) == 0x90U) {
                        if (bytes[2].toUInt() == 0U) {
                            notes.add(NoteEvent(event.tick, NoteEventType.Off, bytes[1]))
                        } else {
                            notes.add(NoteEvent(event.tick, NoteEventType.On, bytes[1]))
                        }
                    }
                }
            }
        }
//    flatNotes(notes)
        return notes
    }

    private fun flatNotes(notes: List<NoteEvent>) {
        notes.forEach { note ->
            note.note = (note.note.toUInt() % 12U).toByte()
        }
    }

    private fun sortNotes(notes: List<NoteEvent>): List<NoteEvent> {
        return notes.sortedWith(object : Comparator<NoteEvent> {
            override fun compare(o1: NoteEvent, o2: NoteEvent): Int {
                if (o1.tick != o2.tick) return if (o1.tick < o2.tick) -1 else 1
                if (o1.type != o2.type) return if (o1.type == NoteEventType.Off) -1 else 1
                return o1.note - o2.note
            }
        })
    }

    private fun extractMelody(notes: List<NoteEvent>, pickMin: Boolean): String {
        val melody = mutableListOf<NoteEvent>()
        val currentNotes = mutableMapOf<Char, NoteEvent>()
        var tick = -1L
        val checkAddMelody = {
            if (currentNotes.isNotEmpty()) {
                val max = currentNotes.maxBy { it.key }
                melody.add(max.value)
                currentNotes.clear()
            }
        }
        notes.forEach { note ->
            if (note.type == NoteEventType.On) {
                if (note.tick != tick) {
                    checkAddMelody()
                }
                currentNotes[note.note.toInt().toChar()] = note
                tick = note.tick
            }
        }
        checkAddMelody()
        val str = melody.joinToString(" ") { it.name.takeIf { it.isNotEmpty() } ?: note2Name(it.note.toInt()) }
        println("Melody:$str")
        return melody.joinToString("") { it.note.toInt().toChar().toString() }
    }

//    private fun extractMelody(notes: List<NoteEvent>, pickMin: Boolean): String {
//        val melody = mutableListOf<NoteEvent>()
//        val currentNotes = mutableMapOf<Char, NoteEvent>()
//        var lastNote: Char = if (pickMin) Char.MAX_VALUE else Char.MIN_VALUE
//        var tick = -1L
//        val checkAddMelody = {
//            if (currentNotes.isNotEmpty()) {
//                val max = if (pickMin) currentNotes.minBy { it.key } else currentNotes.maxBy { it.key }
//                if (max.key != lastNote) {
//                    lastNote = max.key
//                    melody.add(max.value)
//                }
//            }
//        }
//        notes.forEach { note ->
//            if (note.tick != tick) {
//                checkAddMelody()
//            }
//            if (note.type == NoteEventType.On) {
//                currentNotes.put(note.note.toInt().toChar(), note)
//            } else if (note.type == NoteEventType.Off) {
//                currentNotes.remove(note.note.toInt().toChar())
//            }
//            tick = note.tick
//        }
//        val str = melody.joinToString(" ") { it.name.takeIf { it.isNotEmpty() } ?: note2Name(it.note.toInt()) }
//        println("Melody:$str")
//        return melody.joinToString("") { it.note.toInt().toChar().toString() }
//    }

    private fun extractWords(notes: List<NoteEvent>): String {
        val melody = mutableListOf<Char>()
        var tick = -1L
        notes.forEach { note ->
            if (note.type == NoteEventType.On) {
                if (note.tick != tick) {
                    if (tick != -1L) {
                        melody.add(Char.MIN_VALUE)
                    }
                    tick = note.tick
                }
                melody.add(note.note.toInt().toChar())
            }
        }
        return melody.joinToString("")
    }

    private fun extractChords(notes: List<NoteEvent>): String {
        val melody = mutableListOf<Char>()
        var chord: Int = 0
        var tick = -1L
        notes.forEach { note ->
            if (note.type == NoteEventType.On) {
                if (note.tick != tick) {
                    if (chord != 0) {
                        melody.add(chord.toChar())
                        chord = 0
                    }
                    tick = note.tick
                }
                chord = chord or (1 shl (note.note.toInt() % 12))
            }
        }
        if (chord != 0) {
            melody.add(chord.toChar())
            chord = 0
        }
        return melody.joinToString("")
    }

    private fun extractText(notes: List<NoteEvent>): String {
        val melody = mutableListOf<Char>()
        var tick = -1L
        notes.forEach { note ->
            if (note.type == NoteEventType.On) {
                if (note.tick != tick) {
                    if (tick != -1L) {
//                        melody.add(32.toChar())
//                        melody.add(Char.MIN_VALUE)
                    }
                    tick = note.tick
                }
                val code = note.note.toInt() % 12 + 65
                val char = code.toChar()
                melody.add(char)
            }
        }
        return melody.joinToString("")
    }

    fun parseMidi(file: File, type: Int = 0): String {
        return FileInputStream(file).use {
            parseMidi(it, type)
        }
    }

    fun parseMidi(inputStream: InputStream, type: Int = 0): String {
        val notes = readNotesFromMidi(inputStream)
        return parseNotes(notes, type)
    }

    fun parseNotes(notes: List<NoteEvent>, type: Int = 0): String {
        return sortNotes(notes).let {
            printNotes(it)
            when (type) {
                0, 1 -> extractMelody(it, type == 1)
                2 -> extractWords(it)
                3 -> extractChords(it)
                else -> extractText(it)
            }
        }
    }

    fun extractLines(lines: List<Pair<Int, Int>>): String {
        val text = lines.joinToString(" ") {
            if (it.first == 0) "上${it.second}"
            else "下${it.second}"
        }
        logger.warning("text:$text")
        return lines.joinToString("") {
            if (it.first == 0) '8'.plus(max(-23, min(23, it.second))).toString()
            else 'g'.plus(max(-23, min(23, it.second))).toString()
        }
    }

    private fun printNotes(notes: List<NoteEvent>) {
        print("Notes: ")
        notes.filter {
            it.type == NoteEventType.On
        }.forEach {
            print(it.name.takeIf { it.isNotEmpty() } ?: note2Name(it.note.toInt()))
            print(" ")
        }
        println()
    }

    private fun note2Name(note: Int): String {
        val noteNames = arrayOf("C", "D", "E", "F", "G", "A", "B")
        val octave = note / 12 - 1
        val step = (note % 12).let {
            if (it < 5) it / 2
            else (it + 1) / 2
        }
        val name = noteNames[step]
        val alter = (note % 12).let {
            if (it < 5) it % 2
            else (it + 1) % 2
        }

        return "${if (alter == 1) "#" else ""}$name$octave"
    }
}
