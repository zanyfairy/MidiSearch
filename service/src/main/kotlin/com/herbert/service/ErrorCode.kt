package com.herbert.service

import com.fasterxml.jackson.databind.node.JsonNodeFactory
import com.fasterxml.jackson.databind.node.ObjectNode

enum class ErrorCode(val code: Int, val msg: String) {
    Success(0, "成功"),
    UnknownError(-1, "未知错误"),
    AlreadyHasAJob(-10001, "已有任务正在进行"),
    UpdateDataWarehouseFailed(-10002, "更新数据仓库失败"),
    NoPermissionToWriteFile(-10003, "没有写文件权限"),
    DataWarehouseNotReady(-10004, "数据仓库尚未准备好"),
    ;
}

class ResultException(val error: ErrorCode) : RuntimeException(error.msg)

class Result constructor(error: ErrorCode = ErrorCode.Success) {
    private val obj = JsonNodeFactory.instance.objectNode()

    init {
        setErrorCode(error)
    }

    fun obj(): ObjectNode {
        return obj
    }

    fun setErrorCode(error: ErrorCode): ObjectNode {
        obj.put("code", error.code)
        obj.put("msg", error.msg)
        return obj
    }

    override fun toString(): String {
        return obj.toString()
    }
}
