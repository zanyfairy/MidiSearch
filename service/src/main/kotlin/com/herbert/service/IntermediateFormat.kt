package com.herbert.service

import com.herbert.core.MelodyParser
import java.io.StringReader

object IntermediateFormat {
    fun convert2Notes(text: String): List<MelodyParser.NoteEvent> {
        val reader = StringReader(text)
        val lines = reader.readLines()
        val staffClef = arrayOf('G', 'F')
        val keys = arrayOf(mutableMapOf(), mutableMapOf<Int, Int>())
        var time = 0
        val notes = mutableListOf<MelodyParser.NoteEvent>()
        lines.forEach { lineText ->
            val words = lineText.trim().split(' ')
            if (words.isEmpty()) return@forEach
            var staff = 0
            var minDuration = Int.MAX_VALUE
            var noteDuration = 0
            words.forEach { word ->
                if (word == "下") staff = 1
                else if (word == "上") staff = 0
                else if (word.startsWith("*clef")) {
                    if (word == "*clefG2") {
                        staffClef[staff] = 'G'
                    } else if (word == "clefF4") {
                        staffClef[staff] = 'F'
                    }
                } else if (word.startsWith("*k[")) {
                    val str = word.substring(3, word.length - 1)
                    val map = mutableMapOf<Int, Int>()
                    for (i in 0 until str.length / 2) {
                        val key = str.substring(i * 2, i * 2 + 1).lowercase()[0]
                        val step = (key.minus('a') + 5) % 7 + 1
                        val direction = str.substring(i * 2 + 1, i * 2 + 2)
                        if (direction == "#") map[step] = 1
                        else map[step] = -1
                    }
                    keys[staff] = map
                } else if (word.matches(Regex("\\d+"))) {
                    noteDuration = 256 / word.toInt()
                } else if (word == ".") {
                    noteDuration = noteDuration * 3 / 2
                } else if (word == "r") {
                    minDuration = Math.min(minDuration, noteDuration)
                } else if (word.startsWith("pitch_")) {
                    minDuration = Math.min(minDuration, noteDuration)
                    val line = word.substring(6).toInt()
                    val note = calculateNote(staffClef[staff], keys[staff], line)
                    notes.add(
                        MelodyParser.NoteEvent(
                            time.toLong(),
                            MelodyParser.NoteEventType.On,
                            note.first,
                            note.second
                        )
                    )
                    notes.add(
                        MelodyParser.NoteEvent(
                            time.toLong() + noteDuration,
                            MelodyParser.NoteEventType.Off,
                            note.first,
                            note.second
                        )
                    )
                }
            }
            if (minDuration != Int.MAX_VALUE)
                time += minDuration
        }
        return notes
    }

    fun convert2Lines(text: String): List<Pair<Int, Int>> {
        val reader = StringReader(text)
        val lines = reader.readLines()
        val staffClef = arrayOf('G', 'F')
        val keys = arrayOf(mutableMapOf(), mutableMapOf<Int, Int>())
        var time = 0
//        val notes = mutableListOf<MelodyParser.NoteEvent>()
        val noteLines = mutableListOf<Pair<Int, Int>>()
        lines.forEach { lineText ->
            val words = lineText.split(' ')
            if (words.isEmpty()) return@forEach
            var staff = 0
            var minDuration = Int.MAX_VALUE
            var noteDuration = 0
            var last: Pair<Int, Int>? = null
            words.forEach { word ->
                if (word == "下") staff = 1
                else if (word == "上") staff = 0
                else if (word.startsWith("*clef")) {
                    if (word == "*clefG2") {
                        staffClef[staff] = 'G'
                    } else if (word == "clefF4") {
                        staffClef[staff] = 'F'
                    }
                } else if (word.startsWith("*k[")) {
                    val str = word.substring(3, word.length - 1)
                    val map = mutableMapOf<Int, Int>()
                    for (i in 0 until str.length / 2) {
                        val key = str.substring(i * 2, i * 2 + 1).lowercase()[0]
                        val step = (key.minus('a') + 5) % 7 + 1
                        val direction = str.substring(i * 2 + 1, i * 2 + 2)
                        if (direction == "#") map[step] = 1
                        else map[step] = -1
                    }
                    keys[staff] = map
                } else if (word.matches(Regex("\\d+"))) {
                    noteDuration = 256 / word.toInt()
                } else if (word == ".") {
                    noteDuration = noteDuration * 3 / 2
                } else if (word == "r") {
                    minDuration = Math.min(minDuration, noteDuration)
                } else if (word.startsWith("pitch_")) {
                    minDuration = Math.min(minDuration, noteDuration)
                    val line = word.substring(6).toInt()
                    val staffLine = Pair(staff, line)
                    last = last?.let {
                        maxOf(it, staffLine) { o1, o2 ->
                            if (o1.first != o2.first) o2.first - o1.first
                            else o1.second - o2.second
                        }
                    } ?: staffLine
                }
            }
            if (minDuration != Int.MAX_VALUE)
                time += minDuration
            last?.let {
                noteLines.add(it)
            }
        }
        return noteLines
    }

    private val alterNames = mapOf(0 to "", 1 to "#", -1 to "b")
    private val noteNames = arrayOf("C", "D", "E", "F", "G", "A", "B")

    // line 五线谱中间那条线对应的是0
    private fun calculateNote(clef: Char, keys: Map<Int, Int>, line: Int): Pair<Byte, String> {
        return if (clef == 'G') {
            val octave = (line - 1 + 70) / 7 - 5
            val step = (line - 1 + 70) % 7 + 1
            val index = if (step < 4) (step - 1) * 2 else (step - 1) * 2 - 1
            val note = (octave + 1) * 12 + index + (keys[step] ?: 0)
            note.toByte() to "${alterNames[keys[step]] ?: ""}${noteNames[step - 1]}$octave"
        } else {
            val octave = (line + 1 + 70) / 7 - 7
            val step = (line + 1 + 70) % 7 + 1
            val index = if (step < 4) (step - 1) * 2 else (step - 1) * 2 - 1
            val note = (octave + 1) * 12 + index + (keys[step] ?: 0)
            note.toByte() to "${alterNames[keys[step]] ?: ""}${noteNames[step - 1]}$octave"
        }
    }
}
