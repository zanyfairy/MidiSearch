package com.herbert.service

import com.herbert.core.*
import jakarta.annotation.PostConstruct
import jakarta.annotation.PreDestroy
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.launch
import org.springframework.beans.factory.annotation.Value
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.multipart.MultipartFile
import java.io.File
import java.io.FileWriter
import java.util.logging.Logger

@RestController
@RequestMapping
class SearchController {
    private val logger = Logger.getLogger("SearchController")

    @Value("\${dataWarehouse.path}")
    private lateinit var dataWarehouse: String

    @Value("\${dataSource.path}")
    private lateinit var dataSource: String

    private lateinit var midiSearch: MidiSearch

    private var job: Job? = null

    @PostConstruct
    fun init() {
        logger.info("PostConstruct")
        midiSearch = MidiSearch(dataWarehouse)
    }

    @PreDestroy
    fun destroy() {
        logger.info("PreDestroy")
        if (midiSearch.isBuilding()) {
            midiSearch.stopBuild()
        }
    }

    @GetMapping("/checkDisk")
    fun checkDisk(): String {
        try {
            val file = File(dataWarehouse)
            val folder = file.parentFile
            val tmp = File(folder, "tmp.txt")
            FileWriter(tmp).use {
                it.write("hello")
            }
            return Result().toString()
        } catch (e: Exception) {
            return Result(ErrorCode.NoPermissionToWriteFile).toString()
        }
    }

    @GetMapping("/reload")
    fun reload(rebuild: Boolean?): String {
        midiSearch.reload()
        return Result().toString()
    }

    @GetMapping("/rebuild")
    suspend fun rebuild(rebuild: Boolean?): String {
        if (job != null) return Result(ErrorCode.AlreadyHasAJob).toString()
        coroutineScope {
            job = launch {
                midiSearch.buildDataWarehouse(dataSource, rebuild ?: false)
                job = null
            }
        }
        return Result().toString()
    }

    @GetMapping("/update")
    fun update(id: Int, path: String): String {
        if (midiSearch.isBuilding()) return Result(ErrorCode.AlreadyHasAJob).toString()
        val result = midiSearch.updateDataWarehouse(id, path)
        if (result)
            return Result().toString()
        else
            return Result(ErrorCode.UpdateDataWarehouseFailed).toString()
    }

    @GetMapping("/search")
    fun search(text: String): String {
        if (!midiSearch.isReady()) return Result(ErrorCode.DataWarehouseNotReady).toString()
        val results = midiSearch.searchText(text)
        return Result().obj().putPOJO("results", results).toString()
    }

    @PostMapping("/test")
    fun test(@RequestBody data: Map<String, String>): String {
        val text1 = data.get("text1") ?: return Result(ErrorCode.UnknownError).toString()
        val text2 = data.get("text2") ?: return Result(ErrorCode.UnknownError).toString()
        if (!midiSearch.isReady()) return Result(ErrorCode.DataWarehouseNotReady).toString()
        val miniHash1 = LSH.getInstance().calculate(text1.substring(0, Math.min(text1.length, Constants.LshLen)))
        val miniHash2 = LSH.getInstance().calculate(text2.substring(0, Math.min(text2.length, Constants.LshLen)))
        val rate = LSH.getInstance().compare(miniHash1, miniHash2)
        logger.info("rate:$rate")
        return Result().obj().put("rate", rate).toString()
    }

    @GetMapping("/simple")
    fun simple(): String {
        val target =
            "0k4k1dhe4c1c0chb4f4b1fhc4c5j3p;;;9o8668977897o:;><:;;;9888867;;o7<<p7899fifh:;;hkh6g7889j7fjijkjklg434345"
        val like =
            "04k2k1dhc4c1c0chb4f2b1fhc4c3j3co;;;9o886689o777897o::;o<o:;;;;;9o88888677;o;oo77<<p7f899fif9h:;;hkh6g7889j77jijkjklg21234547f899fif9h:;;hkh=g>??@j>>dgd>>?@@hmh;@?@@<<f<<=<<;;j9;;::glg>>?@@hmh;@?@@<<f@"
        val unlike =
            "9;>?<?<?<?<?<?@>;>;>;=;=;=>3i498ei7678h9h<h;9;>?>@k?kB@l?lnl?>=>@B=i>m=;ilnp9l857h8l;7>;989;;989;9897f147f6f45;98878l9;9hlm42h67;989;;989;9897fkbf6745;98k878k9o798n578;B9;B?h?>?>?h@m?@?>>j<>fij>?@kp??"
        if (!midiSearch.isReady()) return Result(ErrorCode.DataWarehouseNotReady).toString()
        val targetHash = LSH.getInstance().calculate(target.substring(0, Math.min(target.length, Constants.LshLen)))
        val likeHash = LSH.getInstance().calculate(like.substring(0, Math.min(like.length, Constants.LshLen)))
        val unlikeHash = LSH.getInstance().calculate(unlike.substring(0, Math.min(unlike.length, Constants.LshLen)))
        val likeRate = LSH.getInstance().compare(targetHash, likeHash)
        val unlikeRate = LSH.getInstance().compare(targetHash, unlikeHash)
        return Result().obj().put("likeRate", likeRate).put("unlikeRate", unlikeRate).toString()
    }

    @PostMapping("/searchIntermediateFile")
    fun searchIntermediateFile(@RequestParam("file") file: MultipartFile): String {
        val str = file.inputStream.readBytes().decodeToString()
        val lines = IntermediateFormat.convert2Lines(str)
        val text = MelodyParser.extractLines(lines)
        logger.info("search length:${text.length} content:$text")
        val results = midiSearch.searchText(text)
        return Result().obj().putPOJO("results", results).toString()
    }

    @PostMapping("/searchCCMZFile")
    fun searchCCMZFile(@RequestParam("file") file: MultipartFile): String {
        val lines = Reader.readCCMZ(file.inputStream, true)?.let { Reader.json2Lines(it) } ?: return "error"
        val text = MelodyParser.extractLines(lines)
        logger.info("search length:${text.length} content:$text")
        val results = midiSearch.searchText(text.substring(0, Constants.LshLen))
        return Result().obj().putPOJO("results", results).toString()
    }
}
